package Projet;
import java.util.*;
import java.lang.Math;


public class ProblemZ2
{
	private LinkedList<Vector2> states;
	private LinkedList<Vector2> exploredStates;
	private LinkedList<Vector2> toExploreState;
	private LinkedList<Vector2> nextIteration;
	private LinkedList<Vector2> actions;
	private LinkedList<Vector2> path;
	private Vector2 startingState;
	private Vector2 finalState;


	//Recoded because the original did reference comparaison instead of value
	protected boolean contains(LinkedList<Vector2> l, Vector2 v)
	{
		for(int i = 0 ; i < l.size() ; i++)
		{
			if(v.equals(l.get(i)))
				return true;
		}
		return false;
	}


	protected boolean contains(LinkedList<Integer> l, Integer v)
	{
		for(int i = 0 ; i < l.size() ; i++)
		{
			if(v.equals(l.get(i)))
				return true;
		}
		return false;
	}

	//Create a successor
	protected Vector2 suc(Vector2 s, Vector2 a)
	{
		return new Vector2(s.x() + a.x(), s.y() + a.y(), s);
	}

}