import Projet.*;
import java.util.*;
import java.lang.Math;
import java.lang.System;

public class Main
{
	public static void main(String[] args) {
		long start = System.nanoTime();
		LinkedList<Vector2> actions = new LinkedList<Vector2>();
		//Unit test
		/*actions.add(new Vector2(0,1)); 
		actions.add(new Vector2(0,-1)); 
		actions.add(new Vector2(1,0)); 
		actions.add(new Vector2(-1,0)); 
		Heuristique1 p = new Heuristique1(new Vector2(1,0), new Vector2(0,257), actions);
		printResult(p.solveBFS(1));*/


		//Second data set
		/*actions.add(new Vector2(-1,1)); 
		actions.add(new Vector2(-1,2)); 
		actions.add(new Vector2(1,-1)); 
		actions.add(new Vector2(2,-1)); 
		Heuristique1 p = new Heuristique1(new Vector2(1,0), new Vector2(0,257), actions);*/

		//Unit test
		/*actions.add(new Vector2(1,0));
		actions.add(new Vector2(0,1));
		Heuristique2 p = new Heuristique2(new Vector2(0,0), new Vector2(10,10), actions);*/

		//Second data set
		/*actions.add(new Vector2(-1,1));
		actions.add(new Vector2(-1,2));
		actions.add(new Vector2(1,-1));
		actions.add(new Vector2(2,-1));
		Heuristique2 p = new Heuristique2(new Vector2(1,0), new Vector2(0,1025), actions);*/

		long finish = System.nanoTime();
		System.out.println("Execution time : " + (finish - start)/1000 + "ms");
		//printResult(p.solveBFS(1));

	}

	//Print the path found
	private static void printResult(LinkedList<Vector2> result)
	{
		if(result == null)
			System.out.println("No solution found");
		else
		{
			System.out.println("Path found :");

			for(int i = 0 ; i < result.size() ; i++)
			{
				System.out.println(result.get(i).toString());
			}
		}
	}
}