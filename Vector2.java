package Projet;
import java.util.*;
import java.lang.Math;

public class Vector2 implements Comparable<Vector2>
{
	private int x;
	private int y;
	//Used for point/state
	private Vector2 parent;

	//Constructors
	public Vector2(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.parent = null;
	}

	public Vector2(int x, int y, Vector2 parent)
	{
		this.x = x;
		this.y = y;
		this.parent = parent;
	}

	public Vector2(Vector2 v, Vector2 u)
	{
		this.x = v.x()+u.x();
		this.y = v.y()+u.y();
		this.parent = null;
	}

	//Getter
	public int x()
	{
		return this.x;
	}

	public int y()
	{
		return this.y;
	}

	public Vector2 parent()
	{
		return this.parent;
	}

	//Setter
	public void parent(Vector2 parent)
	{
		this.parent = parent;
	}

	//Compute magnitude
	public double magnitude()
	{
		return Math.sqrt(x*x + y*y);
	}

	//Check if vectors are colinear (not used)
	public boolean colinear(Vector2 other)
	{
		int crossProduct;
		crossProduct = this.x() * other.x() + this.y() * other.y();
		return crossProduct == 0 ? true : false;
	}

	//Compute the ditance between two point
	public double distance(Vector2 other)
	{
		return Math.sqrt(Math.pow(this.x- other.x(),2) + Math.pow(this.y - other.y(),2));
	}

	//Value comparison of two vector
	public int compareTo(Vector2 other)
	{
		if(this.x == other.x() && this.y == other.y())
			return 1;
		return this.magnitude() < other.magnitude() ? -1 : 1;
	}

	public boolean equals(Vector2 other)
	{
		return (this.x == other.x() && this.y == other.y());
	}

	@Override
	public String toString()
	{
		return "("+String.valueOf(this.x) + "," + String.valueOf(this.y)+")";
	}
}